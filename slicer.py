#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
from random import random
from PIL import Image, ImageDraw


def to_point(x, y, z):
    return "<%f, %f, %f>" % (x, y, z)


SIZE = 10
RADIUS = 0.5

BOX_VOLUME = 8 * SIZE**3
SPHERE_VOLUME = 4 * np.pi / 3 * RADIUS**3


def main(name):
    spheres = []
    if name == "range":
        method = lambda: 0.5 * RADIUS * random() + 0.5*RADIUS
        real_label = "Evenly {:0.3f} ± {:0.3f}".format(3*RADIUS/4.0, RADIUS/4.0)
    elif name == "gaussian":
        method = lambda: np.random.normal(RADIUS, RADIUS/5)
        real_label = "Gaussian {:0.3f} ± {:0.3f}".format(RADIUS, RADIUS/5.0)
    else:
        method = lambda: RADIUS
        real_label = "Monodisperse {:0.3f}".format(RADIUS)
    while SPHERE_VOLUME * len(spheres) * 30 < BOX_VOLUME:
        x = SIZE * random() - 5
        y = SIZE * random() - 5
        z = SIZE * random() - 5
        r = method()

        safe = True

        for (ox, oy, oz, r2) in spheres:
            if (x - ox)**2 + (y - oy)**2 + (z - oz)**2 < (r + r2)**2:
                safe = False
                break

        if safe:
            spheres.append((x, y, z, r))

    slicers = 2 * SIZE * np.random.random(300) - SIZE


    sizes = []

    for idx, slice in enumerate(slicers):
        # Generate the 3D visualisation file
        with open("{}-3d-{}.pov".format(name, idx), "w") as outfile:
            outfile.write("""#include "colors.inc"
        camera {location %s look_at <0, 0, 0>}
        light_source { %s color White}
        polygon {4 %s %s %s %s texture {pigment {color <0.6, 1, 1, 0.3>}}}\n""" % (
                to_point(SIZE, SIZE, 15), to_point(1.7 * SIZE, SIZE, 15),
                to_point(SIZE, 0, SIZE), to_point(-SIZE, 0, SIZE),
                to_point(-SIZE, 0, -SIZE), to_point(SIZE, 0, -SIZE)))

            for (x, y, z, r) in spheres:
                outfile.write("sphere { <%f, %f, %f>, %f "
                            "texture {pigment {color Yellow }} "
                            "finish { ambient rgb<0.2, 0.2, 0.2>}}\n" % (
                                x, y, z, r))

        im = Image.new("RGB", (1000, 1000))
        draw = ImageDraw.Draw(im)
        for (x, y, z, r) in spheres:
            y -= slice  # adjust to slicing plane
            if abs(y) > r:
                continue
            rad = np.sqrt(r**2 - y**2)  # calculate intersecting radius
            sizes.append(rad)
            x += SIZE/2
            z += SIZE/2
            draw.ellipse(((x - rad) * 100, (z - rad) * 100,
                        (x + rad) * 100, (z + rad) * 100),
                        fill='yellow')

        im.save("{}-{}.png".format(name, idx))

    sizes = np.array(sizes)
    size_weights = sizes*0+1.0/len(sizes)
    actual = np.array([r for (_, _, _, r) in spheres])
    actual_weights = actual*0+1.0/len(actual)

    bins = np.arange(0, max([RADIUS+0.025, max(sizes)+0.025]), 0.025)
    plt.figure(figsize=(16,9))
    plt.hist([sizes, actual], bins=bins,
            weights=[size_weights, actual_weights],
            label=["Measured: {:0.3f} ± {:0.3f}".format(np.mean(sizes), np.std(sizes)),
                   "Actual: " + real_label])
    plt.xlabel("Radius")
    plt.ylabel("Relative Proportion")
    plt.legend()
    plt.savefig("{}-histogram.png".format(name),
                facecolor=plt.rcParams["axes.facecolor"])
    plt.clf()

main("gaussian")
main("monodisperse")
main("range")
